FROM postgres:latest
MAINTAINER Kostas Andreadis <kandread@umass.edu>

RUN apt update
RUN apt install -y python3 python3-pip gdal-bin build-essential libgdal-dev postgis

ENV CPLUS_INCLUDE_PATH /usr/include/gdal
ENV C_INCLUDE_PATH /usr/include/gdal
RUN pip3 install numpy netCDF4 pandas scipy requests h5py lxml bs4 pydap psycopg2 climateserv
RUN pip3 install GDAL==$(gdal-config --version) --global-option=build_ext --global-option="-I/usr/include/gdal"

ADD . /rheas
RUN rm /rheas/data/*gz
RUN rm -r /rheas/data/crops
RUN for f in /rheas/data/vic/*.gz; do gunzip $f; done
RUN cd /rheas/external/vic/src && make && cd /
RUN pip3 install -e rheas

RUN apt autoremove
