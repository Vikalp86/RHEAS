FROM postgis/postgis:latest

MAINTAINER kandread@umass.edu

ENV POSTGRES_USER rheas
ENV POSTGRES_PASSWORD rheas
ENV POSTGRES_DB rheas

RUN mkdir -p /docker-entrypoint-initdb.d
COPY scripts/raster-db.sh /docker-entrypoint-initdb.d/1_raster-db.sh
COPY data/crops.sql.gz /docker-entrypoint-initdb.d/2_crops.sql.gz
COPY data/dssat.sql.gz /docker-entrypoint-initdb.d/3_dssat.sql.gz
COPY data/vic.sql.gz /docker-entrypoint-initdb.d/4_vic.sql.gz
COPY data/soils.sql.gz /docker-entrypoint-initdb.d/5_soils.sql.gz
