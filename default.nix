{ pkgs ? import <nixpkgs> {} }:

let
  climateserv = pkgs.python3Packages.buildPythonPackage rec {
    pname = "climateserv";
    version = "0.0.12";

    src = pkgs.python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-T3FraDgsWScnKrL11/gmHpfehC4ex1+xHte6I5n2FBI=";
    };
  };
  gsw = pkgs.python3Packages.buildPythonPackage rec {
    pname = "gsw";
    version = "3.4.0";

    src = pkgs.python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-1FyDWvDlOSNenPRq5Y2UR+IG0UWxPs/nRHrXuPvc+G0=";
    };

    propagatedBuildInputs = with pkgs.python3Packages; [
      numpy
    ];
  };
  coards = pkgs.python3Packages.buildPythonPackage rec {
    pname = "coards";
    version = "1.0.5";

    src = pkgs.python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-+HC2FCiOf5PI9UXLIewn/Vq9DA4lqx9Gd1JKlbp7z9c=";
    };
  };
  pydap = pkgs.python3Packages.buildPythonPackage rec {
    pname = "Pydap";
    version = "3.2.2";

    src = pkgs.python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-hjJmQuJPQhWVp0sPmYbadteTKyd3aPUB/iFNclkr3EA=";
    };

    propagatedBuildInputs = [
      coards
      gsw
      pkgs.python3Packages.beautifulsoup4
      pkgs.python3Packages.numpy
      pkgs.python3Packages.webob
      pkgs.python3Packages.docopt
      pkgs.python3Packages.jinja2
      pkgs.python3Packages.requests
      pkgs.python3Packages.netcdf4
      pkgs.python3Packages.webtest
    ];

    doCheck = false;
  };
  pypi-packages = with pkgs.python3Packages; [
    setuptools
    python
    numpy
    pandas
    gdal
    requests
    scipy
    lxml
    netcdf4
    h5py
    beautifulsoup4
    psycopg2
  ];
  rheas = pkgs.python3Packages.buildPythonPackage rec {
    name = "rheas";
    src = ./.;
    doCheck = false;
    propagatedBuildInputs = [
      pypi-packages
      pydap
      climateserv
    ];
  };
  vic = pkgs.stdenv.mkDerivation {
    name = "vic";
    src = external/vic/src;
    buildInpus = [ ];
    buildPhase = "make";
    installPhase = ''
      mkdir -p $out/bin
      cp vicNl $out/bin/
    '';
  };
  dssat = pkgs.stdenv.mkDerivation {
    name = "dssat";
    src = external/dssat;
    buildInputs= [ ];
    buildPhase = " ";
    installPhase = ''
      mkdir -p $out/bin
      chmod +x mdssat
      cp mdssat $out/bin
    '';
};
in
pkgs.stdenv.mkDerivation {
  name = "rheas-env";
  buildInputs = [
    vic
    dssat
    rheas
    pkgs.glibcLocales       
    pkgs.gdal
    (pkgs.postgresql.withPackages (p: [ p.postgis ]))
  ];
  shellHook = ''
    export PGDATA="$PWD/db"
    export PGHOST="localhost"
    export SOCKET_DIRECTORIES="$PWD/sockets"
    export POSTGRES_DB="rheas"
    if [ ! -d $PGDATA ]; then
        mkdir $SOCKET_DIRECTORIES
        initdb
        echo "unix_socket_directories = '$SOCKET_DIRECTORIES'" >> $PGDATA/postgresql.conf
        pg_ctl -l $PGDATA/logfile start
        createdb $POSTGRES_DB
        echo "CREATE EXTENSION postgis; CREATE EXTENSION postgis_raster;" | psql -d $POSTGRES_DB
        echo "CREATE SCHEMA crops; CREATE SCHEMA dssat; CREATE SCHEMA vic;" | psql -d $POSTGRES_DB
        gunzip < data/crops.sql.gz | psql -d $POSTGRES_DB
        gunzip < data/dssat.sql.gz | psql -d $POSTGRES_DB
        gunzip < data/soils.sql.gz | psql -d $POSTGRES_DB
    else
        pg_ctl -l $PGDATA/logfile start
    fi
    function end {
        echo "Shutting down the database..."
        pg_ctl stop
    }
    trap end EXIT
  '';
}
