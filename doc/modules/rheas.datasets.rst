rheas.datasets package
======================

Submodules
----------

rheas.datasets.amsre module
---------------------------

.. automodule:: rheas.datasets.amsre
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.chirps module
----------------------------

.. automodule:: rheas.datasets.chirps
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.cmorph module
----------------------------

.. automodule:: rheas.datasets.cmorph
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.datasets module
------------------------------

.. automodule:: rheas.datasets.datasets
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.decorators module
--------------------------------

.. automodule:: rheas.datasets.decorators
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.earthdata module
-------------------------------

.. automodule:: rheas.datasets.earthdata
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.gpm module
-------------------------

.. automodule:: rheas.datasets.gpm
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.grace module
---------------------------

.. automodule:: rheas.datasets.grace
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.iri module
-------------------------

.. automodule:: rheas.datasets.iri
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.mcd15 module
---------------------------

.. automodule:: rheas.datasets.mcd15
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.merra module
---------------------------

.. automodule:: rheas.datasets.merra
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.mod10 module
---------------------------

.. automodule:: rheas.datasets.mod10
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.mod16 module
---------------------------

.. automodule:: rheas.datasets.mod16
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.modis module
---------------------------

.. automodule:: rheas.datasets.modis
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.modscag module
-----------------------------

.. automodule:: rheas.datasets.modscag
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.ncep module
--------------------------

.. automodule:: rheas.datasets.ncep
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.nmme module
--------------------------

.. automodule:: rheas.datasets.nmme
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.persiann module
------------------------------

.. automodule:: rheas.datasets.persiann
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.prism module
---------------------------

.. automodule:: rheas.datasets.prism
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.rfe2 module
--------------------------

.. automodule:: rheas.datasets.rfe2
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.smap module
--------------------------

.. automodule:: rheas.datasets.smap
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.smape module
---------------------------

.. automodule:: rheas.datasets.smape
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.smos module
--------------------------

.. automodule:: rheas.datasets.smos
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.snowcover module
-------------------------------

.. automodule:: rheas.datasets.snowcover
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.soilmoist module
-------------------------------

.. automodule:: rheas.datasets.soilmoist
   :members:
   :undoc-members:
   :show-inheritance:

rheas.datasets.trmm module
--------------------------

.. automodule:: rheas.datasets.trmm
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: rheas.datasets
   :members:
   :undoc-members:
   :show-inheritance:
