Installation
=================================

Requirements
--------------------------------
The requirements for installing RHEAS include

* `Python <https://www.python.org>`_ (version 3) and a number of python packages

   * numpy
   * netCDF4
   * GDAL
   * scipy
   * pandas
   * requests
   * h5py
   * lxml
   * beautifulsoup4
   * pydap
   * psycopg2

* `GCC compiler <https://gcc.gnu.org>`_
* `Automake <https://www.gnu.org/software/automake/>`_
* `PostgreSQL <http://www.postgresql.org>`_ server with the `PostGIS <http://postgis.net>`_ extension
* `VIC <http://hydro.washington.edu/Lettenmaier/Models/VIC/>`_ model executable
* `DSSAT <http://dssat.net>`_ model executable

Thankfully most of these requirements can be installed from your OS package manager (Linux and MacOS) and ``pip`` or ``conda``.

In terms of hardware, the recommended requirements include any modern computer system with hard drive storage of at least 250 GB, and memory of at least 4 GB.


Installing on Linux and MacOS
--------------------------------
In order to compile RHEAS, we need a C compiler and the Make utilities, as well as some other software packages. Here we will use a Debian-based distro as an example, but you should be able to find the corresponding packages for other distros.

.. highlight:: bash

::

 sudo apt update
 sudo apt install -y postgresql postgresql-contrib build-essential postgis libgdal-dev gdal-bin python3 python3-pip
 export CPLUS_INCLUDE_PATH=/usr/include/gdal
 export C_INCLUDE_PATH=/usr/include/gdal
 pip3 install numpy netCDF4 pandas scipy requests h5py lxml bs4 pydap psycopg2
 pip3 install GDAL==$(gdal-config --version) --global-option=build_ext --global-option="-I/usr/include/gdal"

The next step involves allowing us to manage the database

.. highlight:: bash

::

  sudo -u postgres createuser -s $(whoami)
  createdb rheas
  echo "create extension postgis;" | psql -d rheas
  echo "create extension postgis_topology;" | psql -d rheas
  echo "create extension postgis_raster;" | psql -d rheas
  echo "create schema vic; create schema dssat; create schema crops;" | psql -d rheas

We then clone the RHEAS repository (will take a while, depending on your internet speed as it's about 2.5GB)

.. highlight:: bash

::

  git clone -b python3 https://gitlab.com/kandread/RHEAS.git
  cd RHEAS

We can now initialize our database with some default data (crop mask, soil information etc.)

.. highlight:: bash

::

  gunzip -c data/crops.sql.gz | psql -d rheas
  gunzip -c data/vic.sql.gz | psql -d rheas
  gunzip -c data/dssat.sql.gz | psql -d rheas
  gunzip -c data/soils.sql.gz | psql -d rheas
  for f in data/vic/*.gz; do gunzip $f; done


Then let's compile VIC

.. highlight:: bash

::

  cd external/vic/src && make && cd -

We also need to set our credentials in order to be able to download data from Earthdata (sign up for an account if you don't have one)

.. highlight:: bash

::

  echo "machine urs.earthdata.nasa.gov login YOURUSERNAME password YOURPASSWORD" >> ~/.netrc

The final step is actually installing RHEAS (almost there!)

.. highlight:: bash

::

  pip3 install -e .
  echo "export PATH=$HOME/.local/bin:$PATH" >> $HOME/.bashrc
  source $HOME/.bashrc

The last line adds the newly installed executables to your path (adjust accordingly if you use a different shell, like zsh)

Let's make sure that we see the help message

.. highlight:: bash

::

   rheas -h


Installing on Windows
--------------------------------
It is currently possible to run RHEAS in a bash-shell environment such as `Cygwin <https://www.cygwin.com/>`_. However, due to backwards compatibility issues with PostGIS dependencies, this method is not currently recommended.


